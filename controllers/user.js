'use strict'

var validator = require('validator');
var User = require('../models/User');


var controller = {

		probando: function(req, res){

		return res.status(200).send({
			message: "Soy el metodo probando"
		});
	},

	testeando: function(req, res){
		return res.status(200).send({
			message: "Soy el metodo Testeando"
		});
	},


save: function(req, res){
		
		//Recoger los parametros
		var params = req.body;
		//Validar los datos
		try{
			var validate_name = !validator.isEmpty(params.name);
			var validate_nameEmpresa = !validator.isEmpty(params.nameEmpresa);
			var validate_email = !validator.isEmpty(params.email) && validator.isEmail(params.email);
			var validate_telefono = !validator.isEmpty(params.telefono);
			var validate_mensaje = !validator.isEmpty(params.mensaje);
			var validate_category = !validator.isEmpty(params.category);


		}catch(err){
			return res.status(200).send({
				message: 'Faltan Datos por enviar'
			});
		}

		if(validate_name && validate_nameEmpresa && validate_email && validate_telefono && validate_mensaje && validate_category){
		//Crear objeto y guardar
		var user = new User();

		//Asignar valores
		user.name = params.name;
		user.nameEmpresa = params.nameEmpresa;
		user.email = params.email;
		user.telefono = params.telefono;
		user.mensaje = params.mensaje;
		user.category = params.category;


		//Guardar el topic
		user.save((err, userStored) =>{

			
			if(err || !userStored){
				return res.status(400).send({
				status: 'error',
				message: 'El Contacto no se ha guardado'
				});

			}


			//Devolver una respuesta
			return res.status(200).send({
			status: 'success',
			user: userStored
});


		});


	
		}else{
			return res.status(400).send({
			message: 'Los datos no son validos'
			});
		}
		
},








};

module.exports = controller;