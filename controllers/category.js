'use strict'

var validator = require('validator');
var Category = require('../models/category');


var controller = {

		probando: function(req, res){

		return res.status(200).send({
			message: "Soy el metodo probando"
		});
	},

	testeando: function(req, res){
		return res.status(200).send({
			message: "Soy el metodo Testeando"
		});
	},




save: function(req, res){
		
		//Recoger los parametros
		var params = req.body;
		//Validar los datos
		try{
			var validate_name = !validator.isEmpty(params.name);
		

		}catch(err){
			return res.status(200).send({
				message: 'Faltan Datos por enviar'
			});
		}

		if(validate_name){
		//Crear objeto y guardar
		var category = new Category();

		//Asignar valores
		category.name = params.name;

		//user.category = req.category.sub;


		//Guardar el Categoria
		category.save((err, categoryStored) =>{

			
			if(err || !categoryStored){
				return res.status(400).send({
				status: 'error',
				message: 'La categoria no se ha guardado'
				});

			}


			//Devolver una respuesta
			return res.status(200).send({
			status: 'success',
			category: categoryStored
});


		});


	
		}else{
			return res.status(400).send({
			message: 'Los datos no son validos'
			});
		}
		
},


getcategory: function(req, res){

	Category.find().exec((err, categorys) =>{

		if(err || !categorys){
			return res.status(404).send({
				status: 'error',
				message: 'No hay categorias que mostrar'
			});
		}
			return res.status(200).send({
				status: 'success',
				categorys
				});

	});
},





};

module.exports = controller;