'use strict'


var express = require('express');
var CategoryController = require('../controllers/category');

var router = express.Router();
//Rutas de prueba
router.get('/prueba', CategoryController.probando);
router.post('/test', CategoryController.testeando);

//Rutas de usuarios
router.post('/registercat', CategoryController.save);
router.get('/category', CategoryController.getcategory);


module.exports = router;